/**
 * add new TODOlist
 * 
 */
function newList() {
  var todoLists = getLocalInfo('todoLists');
  var list = $('.todolist');
  $("<input type='text' placeholder='Enter list name'>").appendTo(list).focus();
  var input = $(list.children('input'));
  input.on('focusout', () => {
    if (input.val() != "") {
      var newList = {
        "id": parseInt(todoLists[todoLists.length - 1].id) + 1,
        "name": input.val()
      };
      list.children('input').remove();
      todoLists.push(newList);
      setLocalInfo('todoLists', todoLists);
      todoLists.push({"id":"all", "name": "All todos"})
      list.repeatable().value = todoLists;
    }
  })  
}

function deleteList(id){
  var list = $('.todolist');
  var todoList = getLocalInfo('todoLists');
  for( let i in todoList){
    if(id != 'all' && todoList[i].id == id ){
      delete todoList[i];
      todoList.clean();
      setLocalInfo('todoLists', todoList);
      todoList.push({"id":"all", "name": "All todos"})
      list.repeatable().value = todoList;
      break;
    }
  }
}

/**
 * add one more todo
 * 
 */
function newOne() {
  var name = $('#todoName').val();
  var date = $('#date').val();
  var list = $('#todolist').val();
  if (name !== "") {
    var todos = getLocalInfo('todos');

    var newTodo = {
      "id": parseInt(todos[todos.length - 1].id) + 1,
      "title": name,
      "listID": list !== undefined ? list : "",
      "date": date ? date : "none",
      "status": "undone",
      "done": false
    };

    $('<li>' + name + '</li>').appendTo('#newTodos').show(100);
    todos.push(newTodo);
    $('#todoName').val("");
    $('#date').val("");
    setLocalInfo('todos', todos);
  }
}

/**
 * change todo status
 * 
 */
function doTodo(elem) {
  var todoId = elem.val();
  var todos = getLocalInfo('todos');
  todos.map((obj) => {
    if (obj.id == todoId) {
      obj.done = obj.done == false ? true : false;
    }
  })
  setLocalInfo('todos', todos);
}

/**
 * delete todo item
 * 
 */
function deleteTodo(elem) {
  // get input element and it's value
  var inputValue = elem.parents('.todoWrapper').find('input').val();
  var todos = getLocalInfo('todos');
  for (let i in todos) {
    if (todos[i].id == inputValue) {
      try {
        // delete current array element
        delete todos[i];
        // remove undefined values
        todos.clean();
        // remove li element
        elem.parents('.doneTodo').remove();
      } catch (err) {
        console.error(err);
      }
      break;
    }
  }
  // rewrite 'todos'
  setLocalInfo('todos', todos);
}

/**
 * update todo name
 */
function updateTodo(el) {
  var todo = el.text();
  var todoId = el.prev('input').val();
  var newTodoName = "";
  el.text("");
  $("<input type='text'>").val(todo).appendTo(el).focus();

  // change todo name on focusout
  $(el.children()).on('focusout', () => {
    var input = el.children('input');
    newTodoName = input.val();
    el.text(newTodoName);
    el.children().remove();
    todo = getLocalInfo('todos');
    for (let i in todo) {
      if (todo[i].id == todoId) {
        todo[i].title = newTodoName;
        break;
      }
    }
    setLocalInfo('todos', todo);
  })
}

/**
 * change todo list
 *
 */
function updateTodoList(el) {
  var currentList = el.text();
  var currListId = el.data('listid');
  var todoLists = getLocalInfo('todoLists');
  var todo = getLocalInfo('todos');
  el.text("");
  var select = '<select>';
  todoLists.unshift({
    "id": "",
    "name": '==Without list=='
  });
  todoLists.map((obj) => {
    select += '<option ' + (currListId === obj.id ? 'selected ' : '') + 'value="' + obj.id + '" >' + obj.name + '</option>';
  })
  select += '</select>';
  $(select).appendTo(el).focus();

  $(el.children()).on('focusout', () => {
    var select = el.children('select');
    var todoId = el.parents('div').find('input').val();

    newListId = select.val();
    if (newListId !== "") {
      todoLists.map((obj) => {
        if (obj.id == newListId) {
          el.text(obj.name);
        }
      })
    } else {
      el.text('without list');
    }

    el.children().remove();
    for (let i in todo) {
      if (todo[i].id == todoId) {
        todo[i].listID = newListId;
        break;
      }
    }
    setLocalInfo('todos', todo);
  })
}

/**
 * change todo date
 * not so usefull
 *
 */
function updateTodoDate(el) {
  var input = '<input type="text" id="date" name="date" data-uk-datepicker="{format:\'DD.MM.YYYY\'}">';
  var todo = el.text();
  var todoId = el.parents('div').find('input').val();
  var newTodoDate = "";
  el.text("");
  $(input).val(todo).appendTo(el).focus();

  // change todo name on focusout
  $('.uk-datepicker-table').on('focusout', () => {
    var input = el.children('input');
    newTodoDate = input.val();
    el.children('input').remove();
    el.text(newTodoDate);

    todo = getLocalInfo('todos');
    for (let i in todo) {
      if (todo[i].id == todoId) {
        todo[i].date = newTodoDate;
        break;
      }
    }
    setLocalInfo('todos', todo);
  })
}

/**
 * sorting functions
 */
function sort(field) {
  var listId = $('#sorted-data').val();
  var data = setListNames(findTodos('', listId));
  console.log(data);
  var res;
  if (field == 'date') {
    res = sortByDate(data);
  } else {
    res = sortByStatus(data);
  }
  console.log(res);
  $('.list-todos').empty();
  var list = $(document).find('.list-todos').repeatable();
  list.value = res;
  checkInput();
}

function sortByDate(arr) {
  var pattern = /(\d{2})\.(\d{2})\.(\d{4})/;
  newArr = arr.sort(function(a, b) {
    return new Date(a.date.replace(pattern, '$3-$2-$1')) - new Date(b.date.replace(pattern, '$3-$2-$1'));
  })
  return newArr;
}

function sortByStatus(arr) {
  console.log('status');
  newArr = arr.sort(function(a, b) {
    return (a.done == b.done) ? 0 : a.done ? -1 : 1;
  })
  return newArr;
}

