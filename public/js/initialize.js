var listnames = getLocalInfo('todoLists') || [],
  todos = getLocalInfo('todoLists') || [];

if (listnames.length === 0 && todos.length === 0) {
  listnames = [{
    'id': 1234,
    'name': 'Work'
  }, {
    'id': 1,
    'name': 'Buy'
  }];
  todos = [{
    'id': 0,
    'title': 'Buy milk',
    'listID': '1',
    'date': 'none',
    'done': true
  }, {
    'id': 1,
    'title': 'gim',
    'listID': '',
    'date': 'none',
    'done': false
  }, {
    'id': 2,
    'title': 'api',
    'listID': '0',
    'date': 'none',
    'done': false
  }, {
    'id': 3,
    'title': 'TODO3',
    'listID': '0',
    'date': 'none',
    'done': false
  }];

  /*global localStorage*/
  setLocalInfo('todoLists', listnames);
  setLocalInfo('todos', todos);
}


Array.prototype.clean = function() {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == undefined) {
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};