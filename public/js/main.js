function checkInput() {
  var inputs = $('.doneTodo').find('input');

  inputs.each(function() {
      if ($(this).data('checked')) {
        $(this).prop('checked', true);
      }
    })
}

function getLocalInfo(index) {
  return JSON.parse(localStorage.getItem(index));
}

function setLocalInfo(index, info) {
  localStorage.setItem(index, JSON.stringify(info));
}

/**
* find elements in array by list ID
*
*/
function findTodos(array, listId) {
  var todos = array || getLocalInfo('todos');
  if (listId == 'all') {
    return todos;
  } else {
    var data = [];
    for (let todo in todos) {
      if (todos[todo].listID == listId) {
        data.push(todos[todo]);
      } else {
        continue;
      }
    }
    return data;
  }
}

/**
* set list names in array
*
*/
function setListNames(arr) {
  var lists = getLocalInfo('todoLists');
  arr.map((obj) => {
    for (var d in lists) {
      if (obj.listID == lists[d].id) {
        obj.listName = lists[d].name;
      } else {
        if (obj.listID === "") {
          obj.listName = "without list";
        }
      }
    }
  });
  return arr;
}